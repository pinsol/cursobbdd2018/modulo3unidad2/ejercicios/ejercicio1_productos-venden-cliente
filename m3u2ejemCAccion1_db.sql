﻿﻿DROP DATABASE IF EXISTS consultaaccion1;
CREATE DATABASE IF NOT EXISTS consultaacion1;
USE consultaacion1;

CREATE TABLE IF NOT EXISTS productos (
  id int AUTO_INCREMENT,
  nombre VARCHAR(30),
  precio float,
  peso float,
  grupo int,
  cantidad int,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS cliente (
  id int AUTO_INCREMENT,
  nombre VARCHAR(30),
  apellidos VARCHAR(30),
  nombreCompleto VARCHAR(60),
  descuento float,
  cantidad int,
  PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS venden (
  productos int,
  cantidad int,
  cliente int,
  fecha date,
  total float,
  PRIMARY KEY (productos,cliente),
  CONSTRAINT FKVendenProductos FOREIGN KEY(productos)
   REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKVendenCliente FOREIGN KEY(cliente)
   REFERENCES cliente(id) ON DELETE CASCADE ON UPDATE CASCADE
);