﻿/* Ejercicio 3 */
UPDATE productos
SET cantidad=0;

UPDATE venden
SET total=0;

UPDATE cliente
SET cantidad=0, nombreCompleto=0;

/* Ejercicio 4 */
  -- c1
SELECT precio p, venden.cantidad c FROM productos JOIN venden ON id=producto;
-- actualizacion
  UPDATE venden JOIN(
SELECT precio p, venden.cantidad c FROM productos JOIN venden ON id=producto
  ) c1 ON c=cantidad
SET total=(SELECT c1.p*c1.c);

SELECT * FROM venden;

  -- c2
SELECT CONCAT_WS(" ",nombre,apellidos) FROM cliente;

UPDATE cliente
SET nombreCompleto=CONCAT_WS(" ",nombre,apellidos);
  
/* Ejercicio 5a */
  -- 
    -- c1
    SELECT precio p, venden.cantidad c, descuento d FROM productos JOIN venden ON id=producto JOIN cliente ON cliente.id=cliente;

    -- Accion
    UPDATE venden JOIN(
       SELECT cliente.id i1,productos.id i2, venden.cantidad c, precio p, descuento d FROM productos JOIN venden ON id=producto JOIN cliente ON cliente.id=cliente)
      c1 ON c1.i1=venden.cliente AND venden.producto=c1.i2
    SET total=c1.p*c1.c*(1-c1.d/100);

    -- 

/* Ejercicio 5b */
    -- c1
    SELECT producto, SUM(cantidad) n FROM venden GROUP BY producto;

    -- Accion
    UPDATE productos JOIN(
    SELECT producto p, SUM(cantidad) n FROM venden GROUP BY producto) c1
      ON p=id
    SET cantidad = n;

    SELECT * FROM productos;

/* Ejercicio 5c */
    -- c1
    SELECT cliente c, SUM(cantidad) n FROM venden GROUP BY cliente;

-- Accion
    UPDATE cliente c JOIN(
    SELECT cliente c, SUM(cantidad) n FROM venden GROUP BY cliente) c1
      ON c=id
    SET cantidad = n;

    SELECT * FROM cliente c;

/* Ejercicio 6a */
   ALTER TABLE productos ADD COLUMN(precioTotal float);

/* Ejercicio 6b */
   ALTER TABLE cliente ADD COLUMN(precioTotal float);

/* Ejercicio 7a */
    -- c1
    SELECT producto p, SUM(total) n FROM venden GROUP BY producto;

-- Accion
    UPDATE productos JOIN(
    SELECT producto p, SUM(total) n FROM venden GROUP BY producto) c1
      ON p=id
    SET precioTotal = n;

    SELECT * FROM productos p;

/* Ejercicio 7b */
    -- c1
    SELECT cliente c, SUM(total) n FROM venden GROUP BY cliente;

-- Accion
    UPDATE cliente c JOIN(
    SELECT cliente c, SUM(total) n FROM venden GROUP BY cliente) c1
      ON c=id
    SET precioTotal = n;

    SELECT * FROM cliente c;
